use std::path::PathBuf;
use serde::Deserialize;
use structopt::StructOpt;
use anyhow::Result;
use anyhow::Error;

#[derive(Debug, StructOpt)]
#[structopt(name = "example", about = "An example of StructOpt usage.")]
struct CLI {
    #[structopt(parse(from_os_str))]
    config: PathBuf,

    #[structopt(short = "v", long = "velocity", default_value = "0")]
    verbose: usize,

    #[structopt(short, long)]
    debug: bool,

    #[structopt(short, long)]
    some_pathes: Option<Vec<PathBuf>>,
}

impl CLI {
    fn merge_conf(&mut self, conf: Conf) {
        if self.some_pathes.is_none() {
            self.some_pathes = Some(conf.some_pathes);
        }
    }
}

#[derive(Debug, Deserialize)]
struct Conf {
    verbose: usize,
    debug: bool,
    some_pathes: Vec<PathBuf>,
}

fn main() -> Result<()> {
    let cli = CLI::from_args();
    let conf = std::fs::read_to_string(cli.config)
        .map_err(Error::from)
        .and_then(|buf| toml::from_str(&buf).map_err(Error::from))?;

    Ok(())
}
